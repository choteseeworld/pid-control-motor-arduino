#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <Ticker.h>

//**********************Constant string communucation**********************

String Direction = "";
String Operand1 = "";
String Operand2 = "";
String Operation = "";
String inputString = "";
bool stringComplete = false;

//********************END constant string communucation**********************



//**********************Network config**********************

const char* ssid = "chote1234";
const char* password = "123456780";
const char* topicReceive = "/ESP/getData"; // topic ชื่อ /server
const char* topicSend = "/ESP8266/RPS";
#define mqtt_server "soldier.cloudmqtt.com" // server
#define mqtt_port 16060                // เลข port
#define mqtt_user "zwwnhpvo"            // user
#define mqtt_password "252lsedkf0FB"    // password
WiFiClient espClient;
PubSubClient client(espClient);

String Re_val_old = "";
int Re_val_new = 0;
String Bt_val_old = "";
int Bt_val_new = 0;
String Topic = "/AC/test";
//********************END network config**********************



//double Kp = 2, Ki = 1, Kd = 2;  // K constant of P, I, D => Kp = 1.5, Ki = 0.5, Kd = 1.5
double Kp, Ki, Kd;



//**********************Constant motor & switch & resister**********************

const int MotLeft = D6;
const int MotRight = D7;
const int MotEnable = D8;
const int EncoderA = D2;

#define resistor A0
#define button D1
//********************END constant motor**********************



//**********************Constant PID controller function**********************

double error = 0;
int setpoint = 0;
double RPS = 0;
double counterValue = 0;
double previous_error = 0;
double integral = 0;
double derivative = 0;
double output = 0;
double pwmMotorValue = 0;

//*******************end constant PID controller function**********************



//*******************Constant string of EPROM**********************
String Operand3 = "";
//*****************END constant string of EPROM**********************

Ticker count_time1; // constant of ticker function



void setup() {

  pinMode(MotEnable, OUTPUT);
  pinMode(MotRight, OUTPUT);
  pinMode(MotLeft, OUTPUT);
  pinMode(EncoderA, INPUT);
  pinMode(resistor, INPUT);
  pinMode(button, INPUT);

  attachInterrupt(digitalPinToInterrupt(EncoderA), countRPS, FALLING);
  attachInterrupt(digitalPinToInterrupt(button), control_btn, CHANGE);

  Serial.begin(115200);
  EEPROM.begin(512);


  //*********************************Conecting network***********************************

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected : ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, mqtt_port); // เชื่อมต่อ mqtt clound
  client.setCallback(callback);             // สร้างฟังก์ชันเมื่อมีการติดต่อจาก mqtt มา

  //*******************************End conecting network***********************************


  Kp = EEPROM.read(0);
  Ki = EEPROM.read(1);
  Kd = EEPROM.read(2);

  count_time1.attach(1, setCounterValue); // Interrupt every 1 second
}

void loop() {
  //**********************Mqtt connection**************************
  if (!client.connected())
  {
    Serial.print("MQTT connecting...");
    // ถ้าเชื่อมต่อ mqtt สำเร็จ
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password))
    {
      client.subscribe(topicReceive); // ชื่อ topic ที่ต้องการติดตาม
      Serial.println("connected");
    }
    // ในกรณีเชื่อมต่อ mqtt ไม่สำเร็จ
    else
    {
      Serial.print("failed, rc= ");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000); // หน่วงเวลา 5 วินาที แล้วลองใหม่
      return;
    }
  }
  client.loop();
  //********************END Mqtt connection************************



  //**********************Serial communucation**********************
  if (Serial.available() > 0)
  {
    char inChar = Serial.read();
    inputString += inChar;
    if (inChar == '\n') //END
    {
      stringComplete = true;
    }
  }

  if (stringComplete)
  {
    Operation = inputString.substring(0, 3);
    //    if (Operation == "stp")       // Set point
    //    {
    //      Operand1 = inputString.substring(3, 4);
    //      Direction = Operand1;
    //      Operand2 = inputString.substring(4);
    //      setpoint = Operand2.toInt();
    //
    //      Serial.print("Direction = ");
    //      Serial.println(Direction);
    //      Serial.print("  Setpoint = ");
    //      Serial.println(setpoint);
    //    }

    Operand3 = inputString.substring(3);
    float Operand4 = Operand3.toFloat();
    if (Operation == "Kp=")
    {
      EEPROM.write(0, Operand4);
      EEPROM.commit();
      Kp = EEPROM.read(0);
    }
    else if (Operation == "Ki=")
    {
      EEPROM.write(1, Operand4);
      EEPROM.commit();
      Ki = EEPROM.read(1);

    }
    else if (Operation == "Kd=")
    {
      EEPROM.write(2, Operand4);
      EEPROM.commit();
      Kd = EEPROM.read(2);

    }

    stringComplete = false;
    inputString = "";
    Serial.print("Kp : ");
    Serial.print(EEPROM.read(0));
    Serial.print("  Ki : ");
    Serial.print(EEPROM.read(1));
    Serial.print("  Kd : ");
    Serial.println(EEPROM.read(2));

  }
  //*******************end serial communucation**********************

}

void control_btn() {
  Bt_val_new = digitalRead(button);
  //  Re_val_new = analogRead(resistor);
  //  if (Re_val_old != String(Re_val_new)) {
  //    client.publish("/ESP8266/Analog", String(Re_val_new).c_str(), true);
  //    Serial.print("Analog : ");
  //    Serial.println(Re_val_new);
  //    Re_val_old = String(Re_val_new);
  //  }
  if (Bt_val_old != String(Bt_val_new)) {
    client.publish("/ESP8266/digital", String(Bt_val_new).c_str(), true);
    Serial.print("Digital : ");
    Serial.println(Bt_val_new);
    Bt_val_old = String(Bt_val_new);
  }
}

void control_resister() {
  //  Bt_val_new = digitalRead(button);
  Re_val_new = analogRead(resistor);
  if (Re_val_old == String(Re_val_new)) {
    //
  }
  client.publish("/ESP8266/Analog", String(Re_val_new).c_str(), true);
  Serial.print("Analog : ");
  Serial.println(Re_val_new);
  Re_val_old = String(Re_val_new);
  //  if (Bt_val_old != String(Bt_val_new)) {
  //    client.publish("/ESP8266/digital", String(Bt_val_new).c_str(), true);
  //    Serial.print("Digital : ");
  //    Serial.println(Bt_val_new);
  //    Bt_val_old = String(Bt_val_new);
  //  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("]");
  String topic_str = topic, payload_str = (char*)payload;
  int i = 0;
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  //Serial.println(payload); // print message for nodejs
  Direction = payload_str.substring(0, 1);
  setpoint = (payload_str.substring(1)).toInt();

}

void setCounterValue() {
  //*******************set number of round per second**********************
  control_resister();
  RPS = counterValue / 2;
  counterValue = 0;
  cvtStr_Publish(RPS);
  error = setpoint - RPS;
  integral = integral + error;
  derivative = (error - previous_error);

  output = output + Kp * error + Kd * derivative + Ki * integral;

  previous_error = error;

  motor_control(output, Direction);
  //****************end set number of round per second**********************

}

void motor_control(double pwmMotorValue, String Direction) {
  if (pwmMotorValue > 1023) {
    pwmMotorValue = 1023;
  }
  
  if (Direction == "L") {
    if (pwmMotorValue < 0) {
      pwmMotorValue = 0;
    }
    digitalWrite(MotLeft, LOW);
    digitalWrite(MotRight, HIGH);
    analogWrite(MotEnable, pwmMotorValue);
  }

  if (Direction == "R") {
    if (pwmMotorValue < 0) {
      pwmMotorValue = 0;
    }
    digitalWrite(MotLeft, HIGH);
    digitalWrite(MotRight, LOW);
    analogWrite(MotEnable, pwmMotorValue);
  }

  Serial.print("  pwmMotor = ");
  Serial.print(pwmMotorValue);
  Serial.print("  Direction: ");
  Serial.print(Direction);
  Serial.print("  setpoint = ");
  Serial.print(setpoint);
  Serial.print("  error = ");
  Serial.print(error);
  Serial.print("  RPS = ");
  Serial.println(RPS);

}

void cvtStr_Publish(double RPS) {
  // publish("/ESP8266/Analog", String(Re_val_new).c_str(), true);
  client.publish("/ESP8266/RPS", String(RPS).c_str(), true);  // publish RPS to MQTT server
}

void countRPS() {
  counterValue++;

}
